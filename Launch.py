import tornado
import tornado.web
import tornado.httpserver
from tornado.options import define, options
import tornado.options
import tornado.httputil
import os
import redis
import time
import redisController
import ConfigParser

from logging import debug, info, warning, error

define("port", default=8080, help="run on the given port", type=int)
define("redis", default='127.0.0.1', help="redis host ip", type=str)

class Application(tornado.web.Application):

    def __init__(self, redisController):

        redisPool = redisController.GetRedisPool()
        self.redisNode = redis.StrictRedis(db=0, connection_pool=redisPool)

        from tornado.httpclient import AsyncHTTPClient
        self.client = AsyncHTTPClient()

        #tornado.ioloop.PeriodicCallback(self.frontpage, 1000*120).start()
        self.frontpage()

        from WebWorker import StartPage, DataService, Config, Admin, Graph, Bubble
        handlers = [
            (r"/admin", Admin),
            (r"/", StartPage),
            (r"/json", DataService),
            (r"/config", Config),
            (r"/bubble", Bubble),
            (r"/graph", Graph)

        ]
        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            debug=True
            )

        tornado.web.Application.__init__(self, handlers, **settings)

    def frontpage(self):
        info('frontpage')

        self.client.fetch('http://reddit.com/.json', self.on_frontpage)

    def on_frontpage(self, response):
        data = response.body
        value = time.time()
        self.redisNode.hset('data', value, data)
        self.localDataPersist(data)

    def localDataPersist(self, data):
        import os
        path = os.path.join('static', 'redisData.json')
        with open(path, 'w') as f:
            f.write(data)


def main():

    tornado.options.parse_command_line()
    configParser = ConfigParser.RawConfigParser()
    configFilePath = r'.\\cfg.default'
    configParser.read(configFilePath)


    try:
        debug('Debug')
        info('Starting our crawler!')
        warning('danger')
        error('ERROR')

        redisControl = redisController.redisController(host=configParser.get('Database', 'IP'), port=configParser.get('Database', 'Port'))
        App = Application(redisControl)
        http_server = tornado.httpserver.HTTPServer(App)
        http_server.listen(options.port)
        info('Listening on port {}'.format(options.port))
        tornado.ioloop.IOLoop.instance().start()

    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.instance().stop()


    #tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
