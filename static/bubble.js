window.data = []


function getnodes() {

    d3.json( '/json',
        function (data) {

            cleandata = []

            for (var key in data) {
                // this is the JS equivalent of `for key in {}`
                if (data.hasOwnProperty(key)) {
                    try {
                        cleandata = cleandata.concat( JSON.parse(data[key]).data.children )
                    }
                    catch (exception) {
                        // some of the data is coming out of Redis as None
                        console.log(key);
                        console.log(data[key]);
                    }
                }
            }

            // this is my hook for feeding things into the browser
            console.log(cleandata)
            render();
        })

}


function render() {

    var margin = 20

    var xscale = d3.scale.linear().domain( [0,window.data.length] ).range( [0+margin,600-margin])

    var yscale = d3.scale.linear()
        .domain( d3.extent(window.data, function(d) {return d.data.ups} ) )
        .range( [300-margin,0+margin])

    var r_attr = 'created'
    var rscale = d3.scale.linear()
        .domain( d3.extent(window.data, function(d) {return d.data[r_attr]}))
        .range( [4, 13])

    var svg = d3.select('svg#drawingbox')


    var circles = svg.selectAll('.post')
        .data( window.data)
        .enter()
        .append('circle')
            .attr('r', function(d) { return rscale( d.data[r_attr]) } )
            .attr('cx', function(d,i) { return xscale(i) })
            .attr('cy', function(d) { return yscale(d.data.ups) })
            .style('fill', 'blue')
            .on('mouseover', function(d) {console.log(d.data)})


}


function init() {

    // kick off first request for data
    getnodes();
}


init()


//$(document).ready(init)
