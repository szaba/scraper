window.data = []

function getnodes() {

    d3.json( '/json',
        function (data) {

            cleandata = []

            for (var key in data) {
                // this is the JS equivalent of `for key in {}`
                if (data.hasOwnProperty(key)) {
                    try {
                        cleandata = cleandata.concat( JSON.parse(data[key]).data.children )
                    }
                    catch (exception) {
                        // some of the data is coming out of Redis as None
                        console.log(key);
                        console.log(data[key]);
                    }
                }
            }

            // this is my hook for feeding things into the browser
            console.log(cleandata)
            render();
        })

}



