import tornado.web

class StartPage(tornado.web.RequestHandler):

    def get(self):
        self.render('index.html')

class Admin(tornado.web.RequestHandler):

    def get(self):

        self.render('admin.html')

class Graph(tornado.web.RequestHandler):
    def get(self):
        self.render('graph.html')

class Bubble(tornado.web.RequestHandler):
    def get(self):
        self.render('bubble.html')

class DataService(tornado.web.RequestHandler):

    def get(self):
        # redisNode = redis.StrictRedis(host='10.0.2.15', port=6379, db=0)

        redisNode = self.application.redisNode

        collectedData = redisNode.hgetall('data')
        self.write(collectedData)

class Config(tornado.web.RequestHandler):


    def setConfig(self, ip, port):
        import ConfigParser
        configParser = ConfigParser.RawConfigParser()
        configParser.read('cfg.default')
        # configParser.set('file', 'IP', ip)
        # configParser.set('file', 'Port', port)
        configParser.set('Database', 'DatabaseType', ip)

        with open('cfg.default', 'wb') as configFile:
            configParser.write(configFile)

    def post(self):

        ip = self.get_body_argument("ServerIP")
        port = self.get_body_argument("ServerPort")
        self.setConfig(ip, port)
        self.write("IP set to: " + ip)
        self.set_status(200,reason=None)
