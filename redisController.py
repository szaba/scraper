import redis


class redisController(object):

    def __init__(self, **redisDetails):

        self.host = '127.0.0.1'
        self.port = 6379
        self.db = 0

        if redisDetails:
            if redisDetails.get('host'):
                self.host = redisDetails.get('host')
            if redisDetails.get('port'):
                self.port = redisDetails.get('port')
            if redisDetails.get('db'):
                self.db = redisDetails.get('db')

        self.pool = redis.ConnectionPool(host=self.host, port=self.port, db=self.db)

    def GetRedisPool(self):
        return self.pool
