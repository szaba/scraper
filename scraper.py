import tornado.ioloop
import urllib2
import json
import tornado.gen
from tornado.httpclient import AsyncHTTPClient
from tornado.concurrent import Future
import futures


class scraper(object):
    def __init__(self):
        self.file = 'savedata.txt'
        self.URLS = ['http://www.reddit.com/r/space/comments/2pflsv/some_perspective_on_exactly_how_far_voyager_1_has.json']

    def loadUrl(self, url, timeout):
        opener = urllib2.build_opener()
        opener.addheaders = [('User-agent', 'Scraper Bot Terrible Name Mk 1')]
        return opener.open(url, timeout=timeout).read()

    def deserializeData(self, dirtyDict):
        cleanDict = []
        for keyword in dirtyDict:
            if not dirtyDict[keyword] == type(dict):
                for item in dirtyDict[keyword]:
                    if not item == type(str):
                        cleanDict[item] = item[dirtyDict[keyword]]
            else:
                cleanDict[keyword] = dirtyDict[keyword]

        return cleanDict

if __name__ == "__main__":
    scrapeMe = scraper()

    with futures.ThreadPoolExecutor(max_workers=5) as executor:
        future_to_url = dict((executor.submit(scrapeMe.loadUrl, url, 60), url)
                             for url in scrapeMe.URLS)

        for future in futures.as_completed(future_to_url):
            url = future_to_url[future]
            if future.exception() is not None:
                print '%r generated an exception: %s' % (url, future.exception())
            else:
                jsonDict = json.loads(future.result())[0]
                # print scrapeMe.deserializeData(jsonDict)
                for key in jsonDict.iteritems():
                    print "Available keys: ", key, "\n"
                print type(jsonDict['data'])
